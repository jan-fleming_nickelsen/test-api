I coded in VS Code with Node.JS, Express, underscore and Joi and used Postman for the GET, PUT etc. requests.
Besides the displayed code, I used the Terminal with the commands:

(> mkdir testAPI

> cd testAPI

> code .)

> npm i express

>npm i underscore

>npm i joi

> nodemon index.js (will automatically open the index.js in the browser)

to install every package needed. When http://localhost:3000/api/cars is active, all example entries are visible and can be accessed by adding /1 , /2 and so on. As mentioned, I have done the GET, POST, PUT and DELETE requests with the desktop version of Postman.
I hope everything is accessable, comprehensive and according to the task.