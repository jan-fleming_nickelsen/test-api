const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());

const cars = [
  { id: 1, model: 'VW Golf 7', license_plate: 'HB_SE_776', seat_count: 4, convertible: false, rating: 8 , engine_type: "fuel"},  
  { id: 2, model: 'Porsche 356a', license_plate: 'HB_AB_123', seat_count: 4, convertible: false, rating: 10 , engine_type: "fuel"},  
  { id: 3, model: 'Tesla Model Y', license_plate: 'HH_EM_111', seat_count: 4, convertible: false, rating: 9 , engine_type: "electric"},  
  { id: 4, model: 'smart fortwo', license_plate: 'M_VB_666', seat_count: 2, convertible: true, rating: 5 , engine_type: "gas"} 
];

//GET-Method

app.get('/api/cars/:id', (req, res) => {
    const car = cars.find(c => c.id === parseInt(req.params.id));
    if (!car) return res.status(404).send('The car with the given ID was not found.');
    res.send(car);
  });

//USE-Method

app.get('/api/cars', (req, res) => {
  res.send(cars);
});

//POST-Method

app.post('/api/cars', (req, res) => {
  const { error } = validateCar(req.body); 
  if (error) return res.status(400).send(error.details[0].message);

  const car = {
    id: cars.length + 1,
    model: req.body.model,
    license_plate: req.body.license_plate,
    seat_count: req.body.seat_count,
    convertible: req.body.convertible,
    rating: req.body.rating,
    engine_type: req.body.engine_type
  };
  cars.push(car);
  res.send(car);
});

//PUT-Method

app.put('/api/cars/:id', (req, res) => {
  const car = cars.find(c => c.id === parseInt(req.params.id));
  if (!car) return res.status(404).send('The car with the given ID was not found.');

  const { error } = validateCar(req.body); 
  if (error) return res.status(400).send(error.details[0].message);
  
  car.model = req.body.model; 
  car.license_plate = req.body.license_plate,
  car.seat_count = req.body.seat_count,
  car.convertible = req.body.convertible,
  car.rating = req.body.rating,
  car.engine_type= req.body.engine_type
  res.send(car);
});

//DELETE-Method

app.delete('/api/cars/:id', (req, res) => {
  const car = cars.find(c => c.id === parseInt(req.params.id));
  if (!car) return res.status(404).send('The car with the given ID was not found.');

  const index = cars.indexOf(car);
  cars.splice(index, 1);

  res.send(car);
});

//Validation

function validateCar(car) {
  const schema = {
    model: Joi.string().min(3).required(),
    license_plate: Joi.string().min(8).required(),
    seat_count: Joi.number().integer().min(1).required(),
    convertible: Joi.required(),
    rating: Joi.number().integer().min(0).max(10).required(),
    engine_type: Joi.required()

  };

  return Joi.validate(car, schema);
}

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));